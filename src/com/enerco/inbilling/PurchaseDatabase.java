/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.enerco.inbilling;

import com.enerco.inbilling.Consts.PurchaseState;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * An example database that records the state of each purchase. You should use
 * an obfuscator before storing any information to persistent storage. The
 * obfuscator should use a key that is specific to the device and/or user.
 * Otherwise an attacker could copy a database full of valid purchases and
 * distribute it to others.
 */
public class PurchaseDatabase {

	
	private static String PRODUCT = "com.enerco.vending.dp_rem_ads";
	
	private static final String TAG = "DrawPad_PurchaseDatabase";
    private static final String DATABASE_NAME = "drawpad.db";
    private static final int DATABASE_VERSION = 1;
    private static final String PURCHASED_ITEMS_TABLE_NAME = "purchased";


    // These are the column names for the "purchased items" table.
    static final String PURCHASED_PRODUCT_ID_COL = "_id";
    static final String PURCHASED_PRODUCT_ID = "product_id";
    static final String PURCHASED_PRODUCT_NAME = "product_name";
    static final String PURCHASED_PRODUCT_STATUS = "status";
    
    private static final String[] PURCHASED_COLUMNS = {
        PURCHASED_PRODUCT_ID_COL, PURCHASED_PRODUCT_ID,
        PURCHASED_PRODUCT_NAME, PURCHASED_PRODUCT_STATUS
    };

    private SQLiteDatabase mDb;
    private DatabaseHelper mDatabaseHelper;

    public PurchaseDatabase(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
        mDb = mDatabaseHelper.getWritableDatabase();
    }

    public void close() {
        mDatabaseHelper.close();
    }

    /**
     * Inserts a purchased product into the database. There may be multiple
     * rows in the table for the same product if it was purchased multiple times
     * or if it was refunded.
     * @param orderId the order ID (matches the value in the product list)
     * @param productId the product ID (sku)
     * @param state the state of the purchase
     * @param purchaseTime the purchase time (in milliseconds since the epoch)
     * @param developerPayload the developer provided "payload" associated with
     *     the order.
     */
    public void insertOrder(String orderId, String productId, 
    		String productName, String state) {
        ContentValues values = new ContentValues();
        values.put(PURCHASED_PRODUCT_ID_COL, orderId);
        values.put(PURCHASED_PRODUCT_ID, productId);
        values.put(PURCHASED_PRODUCT_NAME, productName);
        values.put(PURCHASED_PRODUCT_STATUS, state);
        mDb.replace(PURCHASED_ITEMS_TABLE_NAME, null /* nullColumnHack */, values);
    }

    public boolean checkRegistered() {
    	
    	Cursor c = mDb.query(PURCHASED_ITEMS_TABLE_NAME, PURCHASED_COLUMNS, 
    			PURCHASED_PRODUCT_ID + "=" + "?", new String[]{PRODUCT}, null, null, null);
    	int numRows = c.getCount();
    	
    	c.close();
    	
    	if (numRows > 0) return true;
    	else return false;
    	
    }
    
    

    /**
     * Returns a cursor that can be used to read all the rows and columns of
     * the "purchased items" table.
     */
    public Cursor queryAllPurchasedItems() {
        return mDb.query(PURCHASED_ITEMS_TABLE_NAME, PURCHASED_COLUMNS, null,
                null, null, null, null);
    }

    /**
     * This is a standard helper class for constructing the database.
     */
    private class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            createPurchaseTable(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }

        private void createPurchaseTable(SQLiteDatabase db) {
        	
        	
            db.execSQL("CREATE TABLE " + PURCHASED_ITEMS_TABLE_NAME + "(" +
                    PURCHASED_PRODUCT_ID_COL + " TEXT PRIMARY KEY, " +
                    PURCHASED_PRODUCT_ID + " INTEGER, " + 
                    PURCHASED_PRODUCT_NAME + " TEXT, " +
                    PURCHASED_PRODUCT_STATUS + " TEXT)" 
            );
        }
    }
}
