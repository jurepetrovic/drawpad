package com.enerco.drawpad;

import java.io.Serializable;


public class SerializableMarker implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 463357405419588775L;

	private int longitude;
	private int latitude;
	private String title;
	private String note;

	public SerializableMarker(int longitude, int latitude, String title,
			String note) {
		
		this.longitude = longitude;
		this.latitude = latitude;
		this.title = title;
		this.note = note;
	}

	public int getLongitude() {
		return longitude;
	}

	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
