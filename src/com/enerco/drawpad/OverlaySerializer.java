package com.enerco.drawpad;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;


// Class handling serialization/deserialization of SerializableMarkers which are drawn to the map.


public class OverlaySerializer {
	
	
	protected static final String TAG = "Locator ";
	
	
	// needed for serialization 
	private ArrayList<OverlayItem> overlay;
	private ArrayList<SerializableMarker> serializableOverlay;

	// needed for deserialization
	private byte[] markers;
	private int markersLength;
	
	
	// serialization constructor
	public OverlaySerializer(ArrayList<OverlayItem> overlay) {
		this.overlay = overlay;
		serializableOverlay = new ArrayList<SerializableMarker>();
	}

	//deserialization constructor
	public OverlaySerializer(byte[] markers, int markersLength) {
		this.markers = markers;
		this.markersLength = markersLength;
		serializableOverlay = new ArrayList<SerializableMarker>();
	}
	
	// getters, setters
	
	public ArrayList<OverlayItem> getOverlay() {
		return overlay;
	}
	public void setOverlay(ArrayList<OverlayItem> overlay) {
		this.overlay = overlay;
	}
	
	// serialization methods
	
	public byte[] serialize() {
		
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out;
		
		for(int i=0; i<serializableOverlay.size(); i++) {
		
			try {
			out = new ObjectOutputStream(bos);
			out.writeObject(serializableOverlay.get(i));
			out.flush();
			out.close();
			Log.d(TAG, "Serialized OverlayItem, size: " + bos.size());
			}
			catch (IOException e) {
				Log.d(TAG, "OverlaySerializer::serialize(): IOException");
				Log.d(TAG, e.getMessage());
			}
		}

		return bos.toByteArray();	
	}


	public void convertToSerializable() {

		//SerializableMarker sm = new SerializableMarker(4.12, 3.33, "Abish", "cepec");
		SerializableMarker sm;
		
		for (int i=0; i<overlay.size(); i++) {
			OverlayItem item = overlay.get(i);
			sm = new SerializableMarker(item.getPoint().getLongitudeE6(), item.getPoint().getLatitudeE6(), item.getTitle(), item.getSnippet());
			serializableOverlay.add(sm);
		}
		
	}
	
	// deserialization methods
	
	public ArrayList<OverlayItem> convertToMapable() {
		
		overlay = new ArrayList<OverlayItem>();
		
		SerializableMarker sm;
		OverlayItem item;
		GeoPoint point;
		
		for (int i=0; i<serializableOverlay.size(); i++) {
			sm = serializableOverlay.get(i);
			point = new GeoPoint(sm.getLatitude(), sm.getLongitude());
			item = new OverlayItem(point, sm.getTitle(), sm.getNote());
			overlay.add(item);
		}
		
		return overlay;
	}
	
	public void deserialize() {

		ByteArrayInputStream bis = new ByteArrayInputStream(markers);
		ObjectInputStream in;

		for (int i = 0; i < markersLength; i++) {

			try {
				in = new ObjectInputStream(bis);
				SerializableMarker sm = (SerializableMarker) in.readObject();
				serializableOverlay.add(sm);
				in.close();

			} catch (IOException e) {
				Log.d(TAG, "OverlaySerializer::deserialize(): IOException");
				Log.d(TAG, e.getMessage());

			} catch (ClassNotFoundException e) {
				Log.d(TAG,"OverlaySerializer::deserialize(): ClassNotFoundException");
				Log.d(TAG, e.getMessage());

			}
		}
	}	
	
// end class
}
	
	
