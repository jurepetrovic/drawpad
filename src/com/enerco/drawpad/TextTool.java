package com.enerco.drawpad;


import android.graphics.Paint;
import android.graphics.Paint.Style;



public class TextTool {

	// variable declaration
	
	
	private static final float DEFAULT_TEXT_SIZE = 30;
	private Paint textPaint;
	private String userText;
	
	
	
	
	public TextTool(int color) {

		textPaint = new Paint();
		textPaint.setStyle(Style.FILL_AND_STROKE);
		// text size is fetched from the dialogbox
		textPaint.setTextSize(DEFAULT_TEXT_SIZE);
		// make it normal font
		textPaint.setStrokeWidth(1);
		textPaint.setColor(color);
		textPaint.setAntiAlias(true);
		textPaint.setDither(true);
		userText = "TEXT";
		
		
	}
	


	public String getUserText() {
		return userText;
	}

	public Paint getTextPaint() {
		return textPaint;
	}

	public void setUserText(String userText2) {
		userText = userText2;
	}
	
	
	// set text size
	public void setTextSize(int size) {
		textPaint.setTextSize(size);
	}
	public int getTextSize() {
		return (int) textPaint.getTextSize();
	}

	// set text color
	public void setTextColor(int argb) {
		textPaint.setColor(argb);
	}
	
	 
}
