package com.enerco.drawpad;

public class PointPosition {
	
	public float x;
	public float y;
	
	public PointPosition() {
		// nothing to do
	}
	
	
	public PointPosition(float historicalX, float historicalY) {
		this.x = historicalX;
		this.y = historicalY;
		
	}
	
	

}
