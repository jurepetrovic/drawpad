package com.enerco.drawpad;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import com.enerco.drawpad.DrawView;
import com.enerco.drawpad.R;
import com.enerco.drawpad.dialogs.BrushSizeDialog;
import com.enerco.drawpad.dialogs.ColorPickerDialog;
import com.enerco.drawpad.dialogs.ColorPickerDialogSlider;
import com.enerco.drawpad.dialogs.BrushSizeDialog.OnBrushChangedListener;
import com.enerco.drawpad.dialogs.TextToolDialog;
import com.enerco.drawpad.dialogs.TextToolDialog.OnTextSizeChangedListener;

import com.enerco.inbilling.*;
import com.enerco.inbilling.Consts.PurchaseState;
import com.enerco.inbilling.Consts.ResponseCode;

import com.enerco.inbilling.BillingService.RestoreTransactions;
import com.enerco.inbilling.BillingService.RequestPurchase;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;

import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.Toast;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

public class Drawpad extends Activity implements SensorListener {

	private static String PREFS_NAME = "DrawpadSettings";

	private static final int DIALOG_SAVE_LIST = 0;
	private static final int DIALOG_LOAD_LIST = 1;
	private static final int DIALOG_SET_COLOR_BASIC = 2;
	private static final int DIALOG_SET_BG_COLOR = 3;
	private static final int DIALOG_SET_BRUSH = 4;
	private static final int DIALOG_SET_COLOR_RGB = 5;
	private static final int DIALOG_BRUSH_TYPE = 6;
	private static final int DIALOG_TEXT = 7;

	private static int SHAKE_MODE = 0;
	// color picker: 0 = basic, 1 = RGB
	private static int COLOR_PICKER_MODE = 1;

	private static final int PHOTO_REQUEST = 0;
	private static final int MAP_REQUEST = 1;

	static String filename = "";

	private static String PRODUCT = "com.enerco.vending.dp_rem_ads";

	static int viewMode = 0; // 0 - DrawView, 1- PhotoView

	// logging facility
	private static final String TAG = "DrawPad ";

	// ad references
	// Reference to AdView.
	AdView adView;
	AdRequest req;

	// in billing option in file menu
	private Handler mHandle;
	private BillingService mBillingService;
	private PurchaseDatabase mPurchaseDatabase;
	private DPPurchaseObserver mDPPurchaseObserver;
	// set like this until checked!
	private boolean billingExists = false;
	// registered?
	private boolean regd = false;

	// Queue for last drawing colors used.
	LinkedList<Integer> lastUsedColors;
	// Application preferences (Advanced color picker)
	SharedPreferences prefs;
	// Reference to dialog for switching color dialogs
	Dialog colorDialog;

	private class DPPurchaseObserver extends PurchaseObserver {
		public DPPurchaseObserver(Handler handler) {
			super(Drawpad.this, handler);
		}

		@Override
		public void onBillingSupported(boolean supported) {
			if (Consts.DEBUG) {
				Log.i(TAG, "supported: " + supported);
			}
			if (supported) {
				billingExists = true;
				Log.i(TAG, "In-App Billing is supported");
			} else {
				Log.i(TAG, "In-App Billing is NOT supported");
				billingExists = false;
			}
		}

		@Override
		public void onPurchaseStateChange(PurchaseState purchaseState,
				String itemId, int quantity, long purchaseTime,
				String developerPayload) {
			if (Consts.DEBUG) {
				Log.i(TAG, "onPurchaseStateChange() itemId: " + itemId + " "
						+ purchaseState);
			}

			if (purchaseState == PurchaseState.PURCHASED) {
				// It seems the product is bought.
				setContentView(R.layout.main_regd);

			}

		}

		@Override
		public void onRequestPurchaseResponse(RequestPurchase request,
				ResponseCode responseCode) {
			if (Consts.DEBUG) {
				Log.d(TAG, request.mProductId + ": " + responseCode);
			}
			if (responseCode == ResponseCode.RESULT_OK) {
				if (Consts.DEBUG) {
					Log.i(TAG, "purchase was successfully sent to server");
				}

			} else if (responseCode == ResponseCode.RESULT_USER_CANCELED) {
				if (Consts.DEBUG) {
					Log.i(TAG, "user canceled purchase");
				}

			} else {
				if (Consts.DEBUG) {
					Log.i(TAG, "purchase failed");
				}

			}
		}

		@Override
		public void onRestoreTransactionsResponse(RestoreTransactions request,
				ResponseCode responseCode) {
			if (responseCode == ResponseCode.RESULT_OK) {
				if (Consts.DEBUG) {
					Log.d(TAG, "completed RestoreTransactions request");
				}
				// Update the shared preferences so that we don't perform
				// a RestoreTransactions again.
				SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
				SharedPreferences.Editor edit = prefs.edit();
				// edit.putBoolean(DB_INITIALIZED, true);
				edit.commit();
			} else {
				if (Consts.DEBUG) {
					Log.d(TAG, "RestoreTransactions error: " + responseCode);
				}
			}
		}
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// fullscreen mode
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// Let's instantiate used colors memory
		lastUsedColors = new LinkedList<Integer>();
		for (int i = 0; i < 4; i++) {
			lastUsedColors.add(Color.argb(255, 255, 255, 255));
		}

		// check if we're registered!
		regd = checkRegd();
		// set main app layout
		if (!regd) {
			setContentView(R.layout.main);
		} else if (regd) {
			Log.i(TAG, "We're registered!");
			setContentView(R.layout.main_regd);
		}

		// Let's get sensors
		try {
			if (isDevice()) {
				SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
				sm.registerListener(this, SensorManager.SENSOR_ACCELEROMETER,
						SensorManager.SENSOR_DELAY_NORMAL);
			}
		} catch (Exception e) {
			Log.e(TAG, "Cannot get Accelerometer.");
			Log.e(TAG, e.toString());
		}
		// see view loading.
		viewMode = 0;

		// if we're being restored, we have to draw the old picture
		if (savedInstanceState != null) {

			byte[] img = savedInstanceState.getByteArray("IMG");
			int length = savedInstanceState.getInt("LENGTH");

			Bitmap bmp = LoadFromArray(img, length);
			DrawView drw = (DrawView) findViewById(R.id.drawView);
			Bitmap resized = drw.resizeToFitScreen(bmp);

			drw.LoadBitmap(resized, true);

		}

		// check whether environment is sane :-)
		// check directory structure
		checkFolders();

		// finally load the ad
		// Look up the AdView as a resource and load a request.
		if (!regd) {
			adView = (AdView) this.findViewById(R.id.ad);
			adView.setEnabled(true);
			req = new AdRequest();
			// req.addTestDevice(AdRequest.TEST_EMULATOR); // Emulator
			// req.addTestDevice("FA66C81481719E640F1CFB8E2C76712D");

		}

		// temporary here - let's check if billing is supported.
		mHandle = new Handler();
		mDPPurchaseObserver = new DPPurchaseObserver(mHandle);
		mBillingService = new BillingService();
		mBillingService.setContext(this);

		// Check if billing is supported.
		ResponseHandler.register(mDPPurchaseObserver);
		mBillingService.checkBillingSupported();

	}

	private boolean checkRegd() {
		mPurchaseDatabase = new PurchaseDatabase(this);
		boolean regd = mPurchaseDatabase.checkRegistered();
		mPurchaseDatabase.close();
		return regd;
	}

	private void checkFolders() {

		File path = Environment.getExternalStorageDirectory();
		path = new File(path, "Pictures/");

		if (!path.exists()) {
			try {
				path.mkdir();
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.error_create_folder),
						Toast.LENGTH_SHORT).show();
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.error_folder),
						Toast.LENGTH_SHORT).show();
				Log.e(TAG, "checkFolders(): " + e.getStackTrace());
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);

		return true;

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		// set billing button
		if (billingExists) {
			menu.findItem(R.id.rem_ads).setEnabled(true);
		}

		// set settings button
		prepareColorPickerSetting(menu);

		return super.onPrepareOptionsMenu(menu);
	}

	private void prepareColorPickerSetting(Menu menu) {

		menu.findItem(R.id.advancedColorPickerMode).setChecked(
				prefs.getBoolean("RGB_PICKER", true));

	}

	// menu click handler
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.background:
			showDialog(DIALOG_SET_BG_COLOR);
			return true;
		case R.id.color:
			if (COLOR_PICKER_MODE == 1)
				showDialog(DIALOG_SET_COLOR_RGB);
			else if (COLOR_PICKER_MODE == 0)
				showDialog(DIALOG_SET_COLOR_BASIC);
			return true;
		case R.id.brushType:
			showDialog(DIALOG_BRUSH_TYPE);
			return true;
		case R.id.brushSize:
			showDialog(DIALOG_SET_BRUSH);
			return true;
		case R.id.save:
			// editSaveDialog();
			showDialog(DIALOG_SAVE_LIST);
			return true;
		case R.id.load:
			showDialog(DIALOG_LOAD_LIST);
			return true;
		case R.id.eraser:
			setEraseMode();
			return true;
		case R.id.photo:
			switchPhotoMode2();
			return true;
		case R.id.share:
			sharePic();
			return true;
		case R.id.shareMMS:
			shareMMS();
			return true;
		case R.id.shakeMode:
			toggleShake();
			// handle checkbox
			if (item.isChecked())
				item.setChecked(false);
			else
				item.setChecked(true);
			return true;
		case R.id.clear:
			clearScreen();
			return true;
		case R.id.wallpaper:
			setWallpaper();
			return true;
		case R.id.maps:
			switchMapMode();
			return true;
		case R.id.rem_ads:
			removeAds();
			return true;
		case R.id.exit:
			doExit();
			return true;
		case R.id.advancedColorPickerMode:
			toggleAdvColorPicker();
			// handle checkbox
			if (item.isChecked())
				item.setChecked(false);
			else
				item.setChecked(true);
			return true;
		case R.id.textMode:
			showDialog(DIALOG_TEXT);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void toggleAdvColorPicker() {
		// Choose Color Picker to use
		// Advanced or basic
		// default is advanced!
		COLOR_PICKER_MODE = (COLOR_PICKER_MODE == 1) ? 0 : 1;

		// update prefs.
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("RGB_PICKER", int2bool(COLOR_PICKER_MODE));
		editor.commit();

	}

	private boolean int2bool(int COLOR_PICKER_MODE) {

		return (COLOR_PICKER_MODE == 0) ? false : true;

	}

	private void doExit() {

		super.onDestroy();
		this.finish();

	}

	private void removeAds() {
		// Let's start purchase procedure
		// this is the place where you decide what to buy!

		String mSku = PRODUCT;
		String mPayloadContents = null;

		Log.d(TAG, "Buying: " + mSku);

		if (!mBillingService.requestPurchase(mSku, mPayloadContents)) {
			// showDialog(DIALOG_BILLING_NOT_SUPPORTED_ID);
		}

	}

	// Set the current picture as wallpaper on the phone.
	private void setWallpaper() {

		// get access to view object
		DrawView drw = (DrawView) findViewById(R.id.drawView);

		// create bitmap
		Bitmap bmp = drw.overlayImages();

		// set wallpaper
		try {
			getApplicationContext().setWallpaper(bmp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Toast.makeText(this.getApplicationContext(),
					getResources().getString(R.string.error_wallpaper),
					Toast.LENGTH_SHORT).show();
			Log.e(TAG, "setWallpaper(): " + e.getStackTrace());
		}

		// clean up
		bmp.recycle();

	}

	// Share the PNG file - attach it to intent.
	private void sharePic() {
		// save to file
		boolean ok = SaveFile("share.png");
		if (!ok)
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.error_sdcard),
					Toast.LENGTH_LONG).show();

		if (ok) {
			// create intent and fill data
			Uri uri = Uri.parse("file:///mnt/sdcard/Pictures/share.png");
			Intent intent = new Intent(android.content.Intent.ACTION_SEND);
			// Intent intent = new Intent("android.intent.action.SEND_MSG");
			intent.putExtra(Intent.EXTRA_STREAM, uri);
			intent.setType("image/png");
			intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			intent.putExtra(Intent.EXTRA_SUBJECT,
					getResources().getString(R.string.share_subject));
			startActivity(Intent.createChooser(intent, getResources()
					.getString(R.string.share_title)));
		}
	}

	// share the drawing as MMS. Bypass some ugly HTC Sense undocumented
	// features

	private void shareMMS() {
		// save to file
		boolean ok = SaveFile("share.png");
		if (!ok)
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.error_sdcard),
					Toast.LENGTH_LONG).show();

		if (ok) {
			// create intent and fill data
			try {
				Uri uri = Uri.parse("file:///mnt/sdcard/Pictures/share.png");
				Intent intent = new Intent("android.intent.action.SEND_MSG");
				intent.putExtra(Intent.EXTRA_STREAM, uri);
				intent.setType("image/png");
				intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				intent.putExtra("sms_body",
						getResources().getString(R.string.share_subject));
				startActivity(Intent.createChooser(intent, getResources()
						.getString(R.string.share_title)));
			} catch (Exception e) {
				// Something went wrong with sending MMS.
				Toast.makeText(this.getApplicationContext(),
						getResources().getString(R.string.error_mms),
						Toast.LENGTH_SHORT).show();
				Log.e(TAG, "shareMMS(): " + e.getStackTrace());
			}
		}
	}

	// Let's try again - this time with Activity!
	private void switchPhotoMode2() {

		// startActivityForResult(new Intent(this, PhotoAct.class),
		// PHOTO_REQUEST);

		// check if we've got folder for storage
		checkFolders();

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(new File("/sdcard/Pictures/cam.png")));
		startActivityForResult(intent, PHOTO_REQUEST);
		// viewMode = 1;
	}

	private void setEraseMode() {
		DrawView drw = (DrawView) findViewById(R.id.drawView);
		drw.setColor(drw.bgPaint.getColor());
	}

	private void setSize(int size) {
		DrawView drw = (DrawView) findViewById(R.id.drawView);
		drw.setSize(size);
	}

	private int getSize() {
		DrawView drw = (DrawView) findViewById(R.id.drawView);
		return (int) drw.paint.getStrokeWidth();
	}

	private boolean SaveFile(String filename_arg) {

		File path = Environment.getExternalStorageDirectory();
		path = new File(path, "Pictures/");

		if (!path.exists()) {
			try {
				path.mkdir();
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.error_folder),
						Toast.LENGTH_SHORT).show();
				Log.e(TAG, "SaveFile(): " + e.getStackTrace());
				return false;
			}

		}

		// use argument if exists
		File file;
		if (filename_arg != null)
			file = new File(path, filename_arg);
		else
			file = new File(path, filename);

		try {
			FileOutputStream fileOutStream = new FileOutputStream(file);
			BufferedOutputStream bos = new BufferedOutputStream(fileOutStream);

			DrawView drw = (DrawView) findViewById(R.id.drawView);

			Bitmap bmp = drw.overlayImages();

			bmp.compress(CompressFormat.PNG, 0, bos);

			bos.flush();
			bos.close();

			// clean up
			//bmp.recycle();

			return true;

		} catch (FileNotFoundException e) {
			Log.e(TAG, "SaveFile()::FNF: " + e.getStackTrace());
			return false;

		} catch (IOException e) {
			Log.e(TAG, "SaveFile()::IO: " + e.getStackTrace());
			return false;
		}

	}

	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		switch (id) {
		case DIALOG_SAVE_LIST:
			// save file dialog
			dialog = editSaveDialog();
			break;
		case DIALOG_LOAD_LIST:
			// load file dialog
			dialog = listLoadDialog();
			break;
		case DIALOG_SET_COLOR_BASIC:
			// set color dialog basic
			dialog = listBasicColorDialog();
			break;
		case DIALOG_SET_COLOR_RGB:
			// set color dialog rgb
			dialog = listRGBColorDialog();
			break;
		case DIALOG_SET_BG_COLOR:
			// set background color list
			dialog = listBgColorDialog();
			break;
		case DIALOG_SET_BRUSH:
			// set brush size
			dialog = listBrushDialog();
			break;
		case DIALOG_BRUSH_TYPE:
			// dialog set brush type
			dialog = listBrushTypeDialog();
			break;
		case DIALOG_TEXT:
			// text input dialog
			dialog = textInputDialog();
			break;

		default:
			dialog = null;
		}
		return dialog;
	}

	private Dialog listBrushTypeDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.brush_type);
		builder.setSingleChoiceItems(R.array.brushes, 0,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Which type was selected?
						setBrushType(which);
						Log.d(TAG, "DialogBrushType: " + which);

					}
				});
		builder.setPositiveButton(R.string.ok,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// OK Pressed. Set Brushtype accordingly
						dialog.dismiss();

					}
				});
		builder.setNegativeButton(R.string.cancel,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Cancel pressed. Quit dialog
						dialog.cancel();
					}
				});
		return builder.create();
	}

	// We have to refresh load list each time dialog gets updated
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case DIALOG_LOAD_LIST:
			refreshLoadList(dialog);
			break;
		case DIALOG_SET_COLOR_RGB:
			refreshColorDialog(dialog);
			break;

		}
	}

	private void refreshColorDialog(Dialog dialog) {

		colorDialog = dialog;

		ImageView used_color_1 = (ImageView) dialog
				.findViewById(R.id.used_color_1);
		used_color_1.setBackgroundColor(lastUsedColors.get(3));
		used_color_1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setFgColor(lastUsedColors.get(3));
				dismissColorDialog();

			}
		});

		ImageView used_color_2 = (ImageView) dialog
				.findViewById(R.id.used_color_2);
		used_color_2.setBackgroundColor(lastUsedColors.get(2));
		used_color_2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setFgColor(lastUsedColors.get(2));
				dismissColorDialog();
			}
		});

		ImageView used_color_3 = (ImageView) dialog
				.findViewById(R.id.used_color_3);
		used_color_3.setBackgroundColor(lastUsedColors.get(1));
		used_color_3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setFgColor(lastUsedColors.get(1));
				dismissColorDialog();

			}
		});

		ImageView used_color_4 = (ImageView) dialog
				.findViewById(R.id.used_color_4);
		used_color_4.setBackgroundColor(lastUsedColors.get(0));
		used_color_4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setFgColor(lastUsedColors.get(0));
				dismissColorDialog();
			}
		});
	}

	private void refreshLoadList(Dialog dialog) {

		// just reload the whole dialog.
		dialog = listLoadDialog();
	}

	// set brush size
	private Dialog listBrushDialog() {

		// brush changed handler
		OnBrushChangedListener listener = new OnBrushChangedListener() {

			@Override
			public void brushChanged(int size) {
				setSize(size);
			}
		};
		// create brush changed dialog
		BrushSizeDialog brushes = new BrushSizeDialog(this, listener, getSize());
		return brushes;
	}

	// set background color dialog
	private Dialog listBgColorDialog() {
		final CharSequence[] items = {
				getResources().getString(R.string.color_white),
				getResources().getString(R.string.color_black),
				getResources().getString(R.string.color_red),
				getResources().getString(R.string.color_blue),
				getResources().getString(R.string.color_green),
				getResources().getString(R.string.color_gray),
				getResources().getString(R.string.color_cyan),
				getResources().getString(R.string.color_magenta),
				getResources().getString(R.string.color_yellow),
				getResources().getString(R.string.color_brown) };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.bgcolor_title));
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Toast.makeText(getApplicationContext(), items[item],
				// Toast.LENGTH_SHORT).show();

				boolean color_found = setColor(true, items, item);
				if (!color_found) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.error_bgcolor),
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		AlertDialog alert = builder.create();
		return alert;
	}

	// set color dialogs
	private Dialog listRGBColorDialog() {

		// What happens when color changes
		OnColorChangedListener colorChanged = new OnColorChangedListener() {

			public void colorChanged(int a, int r, int g, int b) {
				// Change new drawing color
				setFgColor(Color.argb(a, r, g, b));

				// Change Text color
				DrawView drw = (DrawView) findViewById(R.id.drawView); 
				drw.drawedText.setTextColor(Color.argb(a, r, g, b));
				
				// add color to used colors and delete if bigger
				if (lastUsedColors.size() == 4)
					lastUsedColors.remove();

				lastUsedColors.add(Color.argb(a, r, g, b));
			}
		};

		// new dialog
		String color = "255,0,0,255";
		return new ColorPickerDialogSlider(Drawpad.this, colorChanged, color,
				lastUsedColors);

	}

	// basic dialog

	private Dialog listBasicColorDialog() {

		// What happens when color changes
		OnColorChangedListener colorChanged = new OnColorChangedListener() {

			public void colorChanged(int a, int r, int g, int b) {
				// Change new drawing color
				setFgColor(Color.argb(a, r, g, b));

				// Change Text color
				DrawView drw = (DrawView) findViewById(R.id.drawView); 
				drw.drawedText.setTextColor(Color.argb(a, r, g, b));
							
				// add color to used colors and delete if bigger
				if (lastUsedColors.size() == 4)
					lastUsedColors.remove();

				lastUsedColors.add(Color.argb(a, r, g, b));
			}
		};

		// arguments are: context, color change handler, default color
		return new ColorPickerDialog(this, colorChanged, Color.BLACK);

	}

	// set color to draw
	// either background or foreground
	private boolean setColor(boolean set_bg, CharSequence[] items, int item) {

		boolean ret = false;
		String color = items[item].toString();
		int colorNumber = -1;

		// not listed colors - custom ones
		if (color == getResources().getString(R.string.color_brown)) {
			colorNumber = Color.rgb(165, 42, 42);
		}

		try {
			if (!set_bg) {
				setFgColor(Color.parseColor(color));
				ret = true;
			} else if (set_bg) {
				setBgColor(Color.parseColor(color));
				ret = true;
			}

		} catch (Exception e) {
			// last resort
			// let's set color from number if there is any.
			if (colorNumber != -1) {
				if (!set_bg) {
					setFgColor(colorNumber);
					ret = true;
				} else if (set_bg) {
					setBgColor(colorNumber);
					ret = true;
				}
			}

		}
		return ret;
	}

	private void setBgColor(int color) {
		DrawView drw = (DrawView) findViewById(R.id.drawView);
		drw.setBgColor(color);
		// Toast.makeText(getApplicationContext(),
		// "Press CLEAR to change background", Toast.LENGTH_SHORT).show();
	}

	private void setFgColor(int color) {
		DrawView drw = (DrawView) findViewById(R.id.drawView);
		drw.setColor(color);
	}

	// load file dialog
	private Dialog listLoadDialog() {

		// check if we've got storage
		checkFolders();

		// this is for file names.

		// load all .png files from SDCARD/Pictures into items.
		File path = Environment.getExternalStorageDirectory();
		path = new File(path, "Pictures/");
		String[] files = path.list();

		if (files == null) {
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.error_load),
					Toast.LENGTH_SHORT).show();
			Log.e(TAG, "listLoadDialog(): line 444");
			return null;

		}

		LinkedList<String> picItems = new LinkedList<String>();

		for (int i = 0; i < files.length; i++) {
			if (files[i].endsWith(".png")) {
				picItems.add(files[i]);
			}
		}

		Log.d(TAG, "listLoadDialog()::num PNG files: " + picItems.size());

		// prepare data for dialog
		final CharSequence[] items = picItems.toArray(new CharSequence[picItems
				.size()]);

		// data preparation failed.
		if (items == null) {
			Log.d(TAG, "listLoadDialog()::Could not prepare file data!");
			return null;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getResources().getString(R.string.load_title));
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				// Toast.makeText(getApplicationContext(), items[item],
				// Toast.LENGTH_SHORT).show();
				filename = items[item].toString();
				boolean loaded = LoadFile();
				if (!loaded) {
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.load_error),
							Toast.LENGTH_SHORT).show();
				}

				dialog.dismiss();
				// make sure the dialog gets destroyed.
				// this guarantees file list will get reloaded next time dialog
				// opens.
				removeDialog(DIALOG_LOAD_LIST);
			}
		});

		AlertDialog alert = builder.create();
		return alert;
	}

	protected boolean LoadFile() {

		boolean ret = false;

		File path = Environment.getExternalStorageDirectory();
		path = new File(path, "Pictures/");

		File file = new File(path, filename);

		if (file.exists()) {

			try {
				FileInputStream fileInStream = new FileInputStream(file);
				BufferedInputStream bis = new BufferedInputStream(fileInStream);
				DrawView drw = (DrawView) findViewById(R.id.drawView);

				Bitmap bmp = drw.resizeToFitScreen(BitmapFactory
						.decodeFile(file.getAbsolutePath()));
				drw.LoadBitmap(bmp, false);

				// clean up
				bmp.recycle();

				bis.close();
				ret = true;

			} catch (FileNotFoundException e) {
				Log.e(TAG, "LoadFile():FileNotFound: " + e.getStackTrace());

			} catch (IOException e) {
				Log.e(TAG, "LoadFile():IO: " + e.getStackTrace());
			}
		}
		return ret;
	}

	public byte[] SaveToArray() {

		// DrawView drw = (DrawView)findViewById(R.id.drawView);
		BasicView drw = (BasicView) findViewById(R.id.drawView);
		Bitmap bm = drw.getImage();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos); // bm is the bitmap
		// object
		byte[] b = baos.toByteArray();

		try {
			baos.flush();
			baos.close();
		} catch (IOException e) {
			// why?
			Log.e(TAG, "SaveToArray():: " + e.getStackTrace());
		}

		return b;

	}

	public Bitmap LoadFromArray(byte[] img, int length) {

		Bitmap bmp = BitmapFactory.decodeByteArray(img, 0, length);
		return bmp;

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (viewMode == 0) {

			super.onSaveInstanceState(outState);
			if (outState != null) {
				byte[] img = SaveToArray();
				outState.putByteArray("IMG", img);
				outState.putInt("LENGTH", img.length);
			}
		}

	}

	@Override
	public void onAccuracyChanged(int sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(int sensor, float[] values) {

		// check mode setting first!
		if (SHAKE_MODE == 1) {

			if (sensor == SensorManager.SENSOR_ACCELEROMETER) {
				// Y - Axis
				if (values[1] > 5) {
					// Toast.makeText(getApplicationContext(),
					// "Screen Cleared (" +
					// values[1] + ")", Toast.LENGTH_SHORT).show();
					DrawView drw = (DrawView) findViewById(R.id.drawView);
					// cannot clear in picture preview mode
					if (viewMode == 0)
						drw.clearCanvas();
					// debug only!!
					// Toast.makeText(getApplicationContext(), "Shaked!",
					// Toast.LENGTH_SHORT).show();

				}
			}
		}

	}

	public void loadPhoto(byte[] data) {

		DrawView drw = (DrawView) findViewById(R.id.drawView);
		// create bitmap from raw picture data
		Bitmap bmp = LoadFromArray(data, data.length);
		// let's resize it

		// Resize and rotate.
		Bitmap bmp2 = drw.resizeAndRotate(bmp);

		// This is for extra cases
		// Bitmap bmp2 = drw.resizeToFitScreen(bmp);

		// clean memory
		bmp.recycle();

		// load it to screen.
		drw.LoadBitmap(bmp2, false);

		bmp2.recycle();
	}

	// create custom dialog for saving files
	private Dialog editSaveDialog() {

		final Dialog dlg = new Dialog(this);

		dlg.setContentView(R.layout.save_dialog);
		dlg.setTitle("Save File");

		Button save = (Button) dlg.findViewById(R.id.dialogButtonOk);
		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				EditText editBox = (EditText) dlg.findViewById(R.id.dialogEdit);
				filename = editBox.getText().toString() + ".png";

				boolean succes = SaveFile(null);
				if (succes) {
					// Toast.makeText(getApplicationContext(),
					// "File Saved: " + filename, Toast.LENGTH_SHORT)
					// .show();
				} else if (!succes)
					Toast.makeText(getApplicationContext(),
							getResources().getString(R.string.save_error),
							Toast.LENGTH_SHORT).show();

				dlg.dismiss();

			}
		});

		return dlg;

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// Toast.makeText(getApplicationContext(),
		// "Key: " + keyCode, Toast.LENGTH_SHORT).show();
		Log.i(TAG, "Key: " + keyCode);

		if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {

			if (viewMode == 0) {
				DrawView drw = (DrawView) findViewById(R.id.drawView);
				drw.clearCanvas();
				return true;
			}

		}

		// back key is for undo or exit if undo is empty
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			DrawView drw = (DrawView) findViewById(R.id.drawView);

			// if there is something in undo, undo it. Otherwise just exit.
			if (!drw.isUndoEmpty())
				drw.doUndo();
			else
				doExit();

			return true;
		}

		return false;

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		// camera operation
		if (requestCode == PHOTO_REQUEST) {

			if (resultCode == Activity.RESULT_CANCELED)
				Toast.makeText(this,
						getResources().getString(R.string.act_cancel_cam),
						Toast.LENGTH_SHORT).show();

			else if (resultCode == Activity.RESULT_OK) {

				// We have to load the picture
				LoadCamFile("cam.png");
				// loadPhoto(data.getByteArrayExtra("pic"));

			}
		}
		// map operation
		if (requestCode == MAP_REQUEST) {
			if (resultCode == Activity.RESULT_CANCELED)
				Toast.makeText(this,
						getResources().getString(R.string.act_cancel_map),
						Toast.LENGTH_SHORT).show();

			else if (resultCode == Activity.RESULT_OK) {

				// We have to load the picture
				LoadCamFile("map.png");
				// loadPhoto(data.getByteArrayExtra("pic"));

			}
		}

	}

	protected boolean LoadCamFile(String fileName) {

		boolean ret = false;

		File path = Environment.getExternalStorageDirectory();
		path = new File(path, "Pictures/");

		File file = new File(path, fileName);

		if (file.exists()) {

			try {

				DrawView drw = (DrawView) findViewById(R.id.drawView);
				// fetch bitmap from file
				Bitmap bmp = loadScaled(file, drw.viewWidth, drw.viewHeight);
				// check orientation
				Bitmap bmp2;

				// check how we're turned.
				if (bmp.getHeight() < bmp.getWidth()) {
					// picture made in landscape
					// downsize first, but keep aspect ratio!
					bmp2 = drw.downsize(bmp, drw.viewHeight, drw.viewWidth);
					Log.d(TAG, "Downsized Width: " + bmp2.getWidth());
					Log.d(TAG, "Downsized Heigth: " + bmp2.getHeight());

					// save memory.
					bmp.recycle();

					// now rotate!
					bmp = drw.rotate(bmp2);
					drw.LoadBitmap(bmp, false);
					ret = true;

				} else {
					bmp2 = drw.resizeToFitScreen(bmp);
					drw.LoadBitmap(bmp2, false);
					ret = true;
				}

				bmp.recycle();
				bmp2.recycle();

			} catch (Exception e) {
				Log.e(TAG, "LoadCamFile():FileNotFound: " + e.getStackTrace());
			}
		}
		return ret;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// check for ads
		if (!regd)
			adView.loadAd(req);

		// notify user how to clear draw area.
		if (viewMode == 0)
			Toast.makeText(this.getApplicationContext(),
					getResources().getString(R.string.act_show),
					Toast.LENGTH_SHORT).show();

		// set to DrawView
		viewMode = 0;

		// Let's get application preferences
		prefs = getSharedPreferences(PREFS_NAME, 0);
		COLOR_PICKER_MODE = (prefs.getBoolean("RGB_PICKER", true)) ? 1 : 0;

	}

	// enable or disable shake to clear functionality
	private void toggleShake() {

		if (SHAKE_MODE == 0) {
			SHAKE_MODE = 1;
			Toast.makeText(this.getApplicationContext(),
					getResources().getString(R.string.shake_on),
					Toast.LENGTH_SHORT).show();

		} else if (SHAKE_MODE == 1) {
			SHAKE_MODE = 0;
			Toast.makeText(this.getApplicationContext(),
					getResources().getString(R.string.shake_off),
					Toast.LENGTH_SHORT).show();

		}

	}

	private void clearScreen() {
		DrawView drw = (DrawView) findViewById(R.id.drawView);
		drw.clearCanvas();
	}

	private Bitmap loadScaled(File path, int desiredWidth, int desiredHeight) {

		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path.getAbsolutePath(), bounds);

		int width = bounds.outWidth;
		int height = bounds.outHeight;

		// Find the correct scale value. It should be the power of 2.
		int scale = 1;

		while (true) {

			if (width / 2 < desiredWidth || height / 2 < desiredHeight)
				break;
			width /= 2;
			height /= 2;
			scale *= 2;
		}

		BitmapFactory.Options resample = new BitmapFactory.Options();
		resample.inSampleSize = scale;

		return BitmapFactory.decodeFile(path.getAbsolutePath(), resample);
	}

	// switch to mapping/locator mode.
	private void switchMapMode() {

		// start intent with Locator class
		startActivityForResult(new Intent(this, Locator.class), MAP_REQUEST);

	}

	private void dismissColorDialog() {

		// I sure hope this is right reference
		colorDialog.dismiss();

	}

	// Let's check if we're running on DEVICE or EMULATOR.
	// If Emulator we're not loading Sensors.

	public boolean isDevice() {
		return !"sdk".equals(Build.MODEL) && !"sdk".equals(Build.PRODUCT)
				&& !"generic".equals(Build.DEVICE);
	}

	private void setBrushType(int type) {
		DrawView drw = (DrawView) findViewById(R.id.drawView);
		drw.setBrushType(type);
		Log.d(TAG, "BrushType: " + type);
	}

	private Dialog textInputDialog() {

		OnTextSizeChangedListener lstn = new OnTextSizeChangedListener() {
			
			@Override
			public void textSizeChanged(int size) {
				// set font size
				DrawView drw = (DrawView) findViewById(R.id.drawView);
				drw.drawedText.setTextSize(size);
			}
			
			@Override
			public void okBtnClicked(Dialog dlg) {
				// Pass the text to TextTool object
				String userText =  ((EditText)dlg.findViewById(R.id.dialogEdit)).getText().toString();
				DrawView drw = (DrawView) findViewById(R.id.drawView);
				drw.drawedText.setUserText(userText);
				// Notify DrawView that we have to drawtext
				drw.TextMode = 1;
				
				// Dismiss Dialog
				dlg.dismiss();
			}
		};
		
		final TextToolDialog dlg = new TextToolDialog(this, lstn, 30 );
		
		return dlg;
	}

}