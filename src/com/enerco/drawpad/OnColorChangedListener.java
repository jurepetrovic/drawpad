package com.enerco.drawpad;

public interface OnColorChangedListener {
	
    void colorChanged(int a, int r, int g, int b);

}