package com.enerco.drawpad;

import android.graphics.Bitmap;

public interface BasicView {
	
	
	public Bitmap getImage();

}
