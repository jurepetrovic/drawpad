package com.enerco.drawpad;

import java.util.ArrayList;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;

import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;


import android.text.ClipboardManager;
import android.util.Log;
import android.view.MotionEvent;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;

import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;

public class MarkerOverlay extends ItemizedOverlay {

	private Locator caller;
	
	private Context mContext;
	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	private Boolean tapMarker = false;
	
	private Paint innerPaint; 
	private Paint borderPaint; 
	private Paint textPaint;
		
	// distinguish between zooming and tapping!
	private Boolean isMotionEvent = false;
	
	
	private GeoPoint tappedPoint;

		
	public MarkerOverlay(Drawable defaultMarker, Boolean tap, Context context) {
		super(boundCenterBottom(defaultMarker));
		this.tapMarker = tap;
		this.mContext = context;
		
	}

	public void setCaller(Locator parent) {
		this.caller = parent;
	}
	
	public ArrayList<OverlayItem> getOverlays() {
		return mOverlays;
	}
	
	
	public void setOverlays(ArrayList<OverlayItem> mOverlays) {
		this.mOverlays = mOverlays;
		populate();
	}

	@Override
	public boolean onTap(GeoPoint p, MapView mapView) {

		// check if it was multitouch and not the actual "tap"
		if (isMotionEvent) {
			isMotionEvent = false;
			return false;
		}

		
		
		Boolean ret = false;
		this.tappedPoint = p;
		
		// If it was the parent that was tapped, do nothing
		try {
			if (super.onTap(p, mapView)) {
				ret = true;
				return ret;
			}
		} catch (Exception e) {
			// grrr....
		}
	
		// if tapMarker == true, draw markers on this overlay.
		if (tapMarker) {
			
			//OverlayItem overlayitem = new OverlayItem(p, "Marker", String.valueOf(mOverlays.size()));
			
			caller.displayNoteDialog();
			ret = true;
			
		}
		return ret;
	}
	
	public void drawNewMarker(String msg) {
		OverlayItem overlayitem = new OverlayItem(tappedPoint, msg, "");
		addOverlay(overlayitem);
	}

	@Override
	public boolean onTap(int index) {

		// An overlay item has been tapped!
		final OverlayItem item = mOverlays.get(index);
		AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet() + mContext.getResources().getString(R.string.loc_lat) + (item.getPoint().getLatitudeE6())/1E6 + mContext.getResources().getString(R.string.loc_lon) +  (item.getPoint().getLongitudeE6())/1E6);
		
		// copy text and cancel buttons
		dialog.setPositiveButton(mContext.getResources().getString(R.string.copy_loc), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// copy location coordinates using ClipboardManager :-)
				caller.addToClipboard(mContext.getResources().getString(R.string.lat) + (item.getPoint().getLatitudeE6())/1E6 + mContext.getResources().getString(R.string.lon) +  (item.getPoint().getLongitudeE6())/1E6);
								
			}
		});
		
		dialog.setNegativeButton(mContext.getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// just close the dialog
				dialog.cancel();
			}
		});
		
		
		dialog.show();
		return true;

	}

	// this is called to find out whether we're being tapped or moved or zoomed.
	
	@Override
	public boolean onTouchEvent(MotionEvent event, MapView mapView) {
	
		if (event.getPointerCount() > 1) isMotionEvent = true;
		return false;
	}
		
	@Override
	protected OverlayItem createItem(int i) {
		// TODO Auto-generated method stub
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return mOverlays.size();
	}
	

	public void addOverlay(OverlayItem overlay) {
	    mOverlays.add(overlay);
	    populate();
	}


	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		// TODO Auto-generated method stub
		super.draw(canvas, mapView, shadow);
		
		Projection proj = mapView.getProjection();
		GeoPoint p = null;
		
	
		// let's draw bubbles over markers.
		if (mOverlays.size() > 0) {
			
			for (int i=0; i<mOverlays.size(); i++) {
				p = mOverlays.get(i).getPoint();
				if (p != null) {
					drawInfoWindow(canvas, proj, p, mOverlays.get(i).getTitle() , mOverlays.get(i).getSnippet(), false);
				}
			}
		}
		
	}	
	
	private void drawInfoWindow(Canvas canvas, Projection myprojection, GeoPoint geopoint, String title, String mesg, boolean shadow) {

			Point point = new Point();
			myprojection.toPixels(geopoint, point);

			// Setup the info window with the right size & location
			int INFO_WINDOW_WIDTH = 150;
			int INFO_WINDOW_HEIGHT = 30;
			RectF infoWindowRect = new RectF(0, 0, INFO_WINDOW_WIDTH,
					INFO_WINDOW_HEIGHT);
			int infoWindowOffsetX = point.x - INFO_WINDOW_WIDTH / 2;
			int infoWindowOffsetY = point.y - INFO_WINDOW_HEIGHT - 50; // bubbleIcon.getHeight();
			infoWindowRect.offset(infoWindowOffsetX, infoWindowOffsetY);

			// Draw inner info window
			canvas.drawRoundRect(infoWindowRect, 5, 5, getInnerPaint());
			// Draw border for info window
			canvas.drawRoundRect(infoWindowRect, 5, 5, getBorderPaint());
			// Draw the MapLocation's name
			int TEXT_OFFSET_X = 10;
			int TEXT_OFFSET_Y = 15;
			
			canvas.drawText(title + mesg, infoWindowOffsetX + TEXT_OFFSET_X, infoWindowOffsetY + TEXT_OFFSET_Y, getTextPaint());
		}
	
	public Paint getInnerPaint() {
		if (innerPaint == null) {
			innerPaint = new Paint();
			innerPaint.setARGB(225, 75, 75, 75); // gray
			innerPaint.setAntiAlias(true);
		}
		return innerPaint;
	}

	public Paint getBorderPaint() {
		if (borderPaint == null) {
			borderPaint = new Paint();
			borderPaint.setARGB(255, 255, 255, 255);
			borderPaint.setAntiAlias(true);
			borderPaint.setStyle(Style.STROKE);
			borderPaint.setStrokeWidth(2);
		}
		return borderPaint;
	}

	public Paint getTextPaint() {
		if (textPaint == null) {
			textPaint = new Paint();
			textPaint.setARGB(255, 255, 255, 255);
			textPaint.setAntiAlias(true);
		}
		return textPaint;
	}


	public void clearMarkers() {
		mOverlays.clear();
	}
	
}
