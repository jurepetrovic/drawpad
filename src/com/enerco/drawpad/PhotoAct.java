package com.enerco.drawpad;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class PhotoAct extends Activity {

	
	private static final int PHOTOVIEW_ID = 1001;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Let's start
		// Switch to fullscreen
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// Switch to landscape for photo bug.
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		// Create photo view
		PhotoView photo = new PhotoView(this.getApplicationContext());
		photo.setId(PHOTOVIEW_ID);
		photo.setAct(this);
		
		setContentView(R.layout.photo);
		
		((FrameLayout) findViewById(R.id.preview)).addView(photo);
	
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		//Toast.makeText(getApplicationContext(),
		//		"Key: " + keyCode, Toast.LENGTH_SHORT).show();
		if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {

				PhotoView photo = (PhotoView) findViewById(PHOTOVIEW_ID);
				photo.takePic();
				return true;
			}
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		
		return false;

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu2, menu);
		return true;

	}
	
	// menu click handler
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.takePhoto:
			takePic();
			return true;			
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void takePic() {
		PhotoView photo = (PhotoView) findViewById(PHOTOVIEW_ID);
		photo.takePic();
	}
}
