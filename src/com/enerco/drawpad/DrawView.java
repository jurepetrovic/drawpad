 package com.enerco.drawpad;


import java.util.Random;
import java.util.Stack;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;


import android.graphics.PorterDuff;

import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;

import android.util.AttributeSet;
import android.util.Log;

import android.view.Display;

import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

public class DrawView extends View implements BasicView {

	protected static final float BRUSH_SIZE = 5;
	protected static final int UNDO_SIZE = 4;
	
	// logging facility
	private static final String TAG = "DrawPad ";
	
	
	// background image
	Bitmap bmp;
	// foreground image
	Bitmap bmpFg;
	
	// Foreground paint
	Paint paint;
	// Background paint
	Paint bgPaint;
	// Spray paint
	Paint sprayPaint;
	
	// canvas for drawing and holding the bitmap
	Canvas c;
	// canvas for foreground drawing
	Canvas fg;

	// empty undo count
	int undoEmptyCnt = 0;
	
	
	// last fetched position
	private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;
	
	
	// path for drawing bezier
	Path path;
	// Stack for undo.
	Stack<Bitmap> undos = new Stack<Bitmap>();

	boolean ArrayDrawed;

	int screenWidth;
	int screenHeight;

	int viewWidth;
	int viewHeight;

	int AD_SIZE = 50;
	
	Context context;

	// Variables for airbrush
	int brushSize;
	int brushType; // 0 = normal, 1 = airbrush
	Random ran;
	
	// Variables for Drawing Text
	// Text object
	public TextTool drawedText;
	// Text mode set from TextTool dialogbox
	public int TextMode = 0;
	
	
	// gets called when inflating from XML
	public DrawView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		
		
		Display display = ((WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		screenWidth = display.getWidth();
		screenHeight = display.getHeight();

		viewWidth = screenWidth;
		viewHeight = screenHeight - AD_SIZE;

		// create Bitmap
		bmp = Bitmap.createBitmap(viewWidth, viewHeight,
				Bitmap.Config.ARGB_8888);
		// foreground bitmap
		bmpFg = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);

		
		// prepare Paint
		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setDither(true);

		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(BRUSH_SIZE);
		// round line cap
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStrokeJoin(Paint.Join.ROUND);

		// prepare background paint
		bgPaint = new Paint();
		bgPaint.setColor(Color.WHITE);

		// Prepare spray paint
		sprayPaint = new Paint();
		sprayPaint.setColor(Color.BLUE);
		sprayPaint.setStrokeWidth(1);
		
		brushType = 0; // 0 = normal, 1=airbrush
		brushSize = 5;
		
		// create path for bezier curves
		path = new Path();

		// Mark first array as not drawed yet
		ArrayDrawed = false;

		// create canvas
		c = new Canvas(bmp);
		// fg canvas
		fg = new Canvas(bmpFg);
		
		// make it white.
		cleanBg();
		clearCanvas();

		ran = new Random();
		
		// text object instantiation
		drawedText = new TextTool(paint.getColor());
		
		
	}

	@Override
	protected void onDraw(Canvas canvas) {

		super.onDraw(canvas);
		canvas.drawBitmap(bmp, 0, 0, null);
		// fg draw
		canvas.drawBitmap(bmpFg, 0, 0, null);
		
		
	}

	
	private void addUndoPoint() {
		
		try {
		
		if (undos.size() > UNDO_SIZE) undos.remove(0);
		undos.push(bmpFg.copy(bmpFg.getConfig(), true));
		}
		catch (Exception e) {
			Log.e(TAG, "addUndoPoint()::Couldn't add undo!");
		}
	}

	
	public void cleanBg() {
		c.drawColor(bgPaint.getColor());
		invalidate();
	}
	
	public void clearCanvas() {

		// foreground bitmap is transparent
		fg.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
		
		// clean undo history
		undos.clear();
		// force redraw
		invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

		setMeasuredDimension(viewWidth, viewHeight);
	}

	
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            	// add undo
                addUndoPoint();
            	//draw text
            	if (TextMode == 1) {
            		drawText(x, y);
            		invalidate();
            		break;
            	}
            	else touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
              
            	//draw text
            	if (TextMode == 1) break;
            	
            	// draw lines
            	
            	else if (brushType == 0) touch_move(x, y);
                else if (brushType == 1) sprayBrush(x, y, brushSize*brushSize*4, brushSize);
                
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
            	
            	// If we're in text mode
            	if (TextMode == 1) {
            		TextMode = 0;
            		break;
            	}
            	
            	// If we're doing normal brush
            	else if (brushType == 0) touch_up();
                invalidate();
                break;
        }
        return true;
    }
	

	private void touch_start(float x, float y) {
        path.reset();
        path.moveTo(x, y);
        mX = x;
        mY = y;
    }
    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            path.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;
            
            // commit to screen
            fg.drawPath(path, paint);
           
            
        }
    }
    private void touch_up() {
        path.lineTo(mX, mY);
        
        // commit to screen
        fg.drawPath(path, paint);
        // kill the path so we don't double draw
        path.reset();
    }

    
    private void sprayBrush(float x, float y, int dotsToDraw, double brushRadius) {
 	
    	
    	 // in case we're far apart, draw a dot in between
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
    	
    	    for (int i = 0; i < dotsToDraw; i++){

    	        // Get the location to draw to
    	        float spray_x = (float) (x + ran.nextGaussian() * brushRadius);
    	        float spray_y = (float) (y + ran.nextGaussian() * brushRadius);

    	       
    	     // Draw the point, using the random color, and the X/Y value
    	        fg.drawPoint(spray_x, spray_y, sprayPaint);
    	        
    	        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
    	        	float spray_mx = (float) (((mX+x)/2) + ran.nextGaussian() * brushRadius);
        	        float spray_my = (float) (((mY+y)/2) + ran.nextGaussian() * brushRadius);
	
        	        // draw the point in between
        	        fg.drawPoint(spray_mx, spray_my, sprayPaint);
        	        
    	        }
    	    
    	    }
    	    // new position
        	mX = x;
        	mY = y;
 	}    
    
    
	public void LoadBitmap(Bitmap loadedBmp, boolean redraw) {
		this.bmp = loadedBmp.copy(loadedBmp.getConfig(), true);

		// clean memory
		loadedBmp.recycle();
		
		if (!this.bmp.isMutable()) {
			writeToast(getResources().getString(R.string.warning_not_mutable));
		} else if (this.bmp.isMutable() && !redraw) {
			writeToast(getResources().getString(R.string.load_ok));
		}

		c.setBitmap(bmp);
	
		invalidate();
			
	}

	private void writeToast(String string) {
		Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
	}

	// to change color from menu
	public void setColor(int color) {
		paint.setColor(color);
		sprayPaint.setColor(color);
	}

	// to change background color from menu
	public void setBgColor(int color) {
		bgPaint.setColor(color);
		c.drawColor(bgPaint.getColor());
		invalidate();
	}

	// to change pen size
	public void setSize(int size) {
		paint.setStrokeWidth(size);
		brushSize = size;
	}

	public Bitmap resizeToFitScreen(Bitmap bmp2) {

		// copy to get mutable object
		//Bitmap orig = bmp2.copy(bmp2.getConfig(), true);

		// clean memory
		//bmp2.recycle();
		
		// Let's do this operation with caution
		Bitmap resizedBmp = null;
		try {
			
			//?????
			resizedBmp = Bitmap.createScaledBitmap(bmp2, viewWidth, viewHeight,
					false);
			
			
		} catch (Exception e) {
			Log.e(TAG, "resizeToFitScreen()::line 351");
		}
		
		
		//clean memory
		//bmp2.recycle();
		
		
		return resizedBmp;

	}

	// resize and rotate 90 right
	public Bitmap resizeAndRotate(Bitmap bmp2) {

		// copy to get mutable object
		Bitmap orig = bmp2.copy(bmp2.getConfig(), true);

		// clean memory
		bmp2.recycle();
		
		// let's get view size (approx.)

		Matrix mx = new Matrix();
		mx.postRotate(90);

		// Let's do this operation with caution
		Bitmap resizedBmp = null;
		try {

			resizedBmp = Bitmap.createBitmap(orig, 0, 0, orig.getWidth(), orig.getHeight(), null, true);

		} catch (Exception e) {
			Log.e(TAG, "resizeAndRotate()::createBitmap()");
			Log.e(TAG, e.getMessage());
		}

		//clean memory
		orig.recycle();
		
		return resizeToFitScreen(resizedBmp);

	}
	
	// check if undo is empty to display exit option
	public boolean isUndoEmpty() {

		if (undos.size() <= 0) {
			undoEmptyCnt++;
			if (undoEmptyCnt == 1) writeToast(getResources().getString(R.string.undo_empty_exit));
			if (undoEmptyCnt == 2) writeToast(getResources().getString(R.string.undo_exiting));
		}
		// reset warning counter
		else if (undos.size() > 0) undoEmptyCnt = 0;
		
		if (undoEmptyCnt > 1) {
			undoEmptyCnt = 0;
			return true;	
		}
		
		return false;
	}
	

	public void doUndo() {

		try {
			bmpFg = undos.pop();
			// fg = foreground canvas
			fg.setBitmap(bmpFg);
 
	
		} catch (Exception e) {
			//writeToast(getResources().getString(R.string.undo_empty_exit));
				
		}
		invalidate();
	}

	public Bitmap overlayImages() {
		Bitmap overlay = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(overlay);
		canvas.drawBitmap(bmp, new Matrix(), null);
		canvas.drawBitmap(bmpFg, new Matrix(), null);
		return overlay;

	}
	
	public Bitmap downsize(Bitmap bmp2, int width, int height) {

		// copy to get mutable object
		Bitmap orig = bmp2.copy(bmp2.getConfig(), true);

		// clean memory
		bmp2.recycle();
		

		// Let's do this operation with caution
		Bitmap resizedBmp = null;
		try {
			resizedBmp = Bitmap.createScaledBitmap(orig, width, height,
					false);
		} catch (Exception e) {
			Log.e(TAG, "downsize()::Exception");
			Log.e(TAG, e.getMessage());
		}
		
		//clean memory
		orig.recycle();
		
		
		return resizedBmp;

	}
	
	public Bitmap rotate(Bitmap bmp2) {

		// copy to get mutable object
		Bitmap orig = bmp2.copy(bmp2.getConfig(), true);

		bmp2.recycle();
		
		// let's get view size (approx.)

		Matrix mx = new Matrix();
		mx.postRotate(90);

		// Let's do this operation with caution
		Bitmap resizedBmp = null;
		try {

			resizedBmp = Bitmap.createBitmap(orig, 0, 0, orig.getWidth(), orig.getHeight(), mx, true);

		} catch (Exception e) {
			Log.e(TAG, "rotate()::createBitmap()");
			Log.e(TAG, e.getMessage());
		}

		// clean memory
		orig.recycle();
		
		return resizedBmp;

	}

	@Override
	public Bitmap getImage() {
		return bmp;
	}
	
	public void setBrushType(int type) {
		// Set Brush type
		brushType = type;
	}
	
	public void drawText(float x, float y) {
				
		fg.drawText(drawedText.getUserText(), x, y, drawedText.getTextPaint());
		
	}
	
}
