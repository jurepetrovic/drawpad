package com.enerco.drawpad.dialogs;

import com.enerco.drawpad.R;

import android.os.Bundle;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class BrushSizeDialog extends Dialog {

	public interface OnBrushChangedListener {
		void brushChanged(int size);
	}

	protected static final int MAX_BRUSH_SIZE = 20;
	protected static final int MIN_BRUSH_SIZE = 1;

	private OnBrushChangedListener mListener;
	private int mInitialSize;
	private Button inc;
	private Button dec;
	private Button ok;
	protected EditText sizeBox;

	public BrushSizeDialog(Context context, OnBrushChangedListener listener,
			int initialSize) {
		super(context);

		mListener = listener;
		mInitialSize = initialSize;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.brush_dialog);
		setTitle(this.getContext().getResources().getString(R.string.brush_dlg_title));

		inc = (Button) findViewById(R.id.dialogButtonPlus);
		dec = (Button) findViewById(R.id.dialogButtonMinus);
		ok = (Button) findViewById(R.id.dialogButtonBrushOK);
		sizeBox = (EditText) findViewById(R.id.dialogEditSize);
		sizeBox.setText(String.valueOf(mInitialSize));

		initButtons();
	}

	private void initButtons() {

		inc.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mInitialSize < MAX_BRUSH_SIZE)
					mInitialSize++;
				sizeBox.setText(String.valueOf(mInitialSize));
				mListener.brushChanged(mInitialSize);

			}
		});

		dec.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mInitialSize > MIN_BRUSH_SIZE)
					mInitialSize--;
				sizeBox.setText(String.valueOf(mInitialSize));
				mListener.brushChanged(mInitialSize);
			}
		});
		
		// close dialog box
		ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		
	}
}
