package com.enerco.drawpad.dialogs;

import com.enerco.drawpad.R;

import android.os.Bundle;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class TextToolDialog extends Dialog {
	
	public interface OnTextSizeChangedListener {
		void textSizeChanged(int size);
		void okBtnClicked(Dialog dlg);
	}
	
	protected static final int MAX_TEXT_SIZE = 60;
	protected static final int MIN_TEXT_SIZE = 15;

	private OnTextSizeChangedListener mListener;
	private int mInitialSize;
	private Dialog mDialog;
	
	private EditText textSizeBox;
	private Button inc;
	private Button dec;
	private Button ok;
	

	public TextToolDialog(Context context, OnTextSizeChangedListener lstn, int initSize) {
		super(context);
	
		mListener = lstn;
		mInitialSize = initSize;
		mDialog = this;
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.text_dialog);
		setTitle(this.getContext().getResources().getString(R.string.textDialogTitle));

		textSizeBox = (EditText) findViewById(R.id.textDialogEditSize);
		textSizeBox.setText(String.valueOf(mInitialSize));

		inc = (Button) findViewById(R.id.textDialogButtonPlus);
		dec = (Button) findViewById(R.id.textDialogButtonMinus);
		ok = (Button) findViewById(R.id.textDialogButtonOk);
		
		initButtons();
	}
	
	private void initButtons() {

		inc.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mInitialSize < MAX_TEXT_SIZE)
					mInitialSize++;
				textSizeBox.setText(String.valueOf(mInitialSize));
				mListener.textSizeChanged(mInitialSize);

			}
		});

		dec.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (mInitialSize > MIN_TEXT_SIZE)
					mInitialSize--;
				textSizeBox.setText(String.valueOf(mInitialSize));
				mListener.textSizeChanged(mInitialSize);
			}
		});
		
		// close dialog box
		ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mListener.okBtnClicked(mDialog);
			}
		});
		
	}
	

}
