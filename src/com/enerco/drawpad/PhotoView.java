package com.enerco.drawpad;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.hardware.Camera;

import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.view.SurfaceView;
import android.view.SurfaceHolder;

import android.widget.Toast;

public class PhotoView extends SurfaceView implements SurfaceHolder.Callback {

	
	// logging facility
	private static final String TAG = "DrawPad ";
	
	Context context;
	
	Activity act;
	
	int screenWidth;
	int screenHeight;

	int viewWidth;
	int viewHeight;

	boolean mPreviewRunning;

	/** Handle to the surface manager object we interact with */
	private SurfaceHolder mSurfaceHolder;
	// camera object
	public Camera camera;

	
	public void setAct(Activity act) {
		this.act = act;
	} 
	
	public PhotoView(Context context, AttributeSet attrs) {
		super(context, attrs);

		this.context = context;
		Display display = ((WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		screenWidth = display.getWidth();
		screenHeight = display.getHeight();

		viewWidth = screenWidth;
		viewHeight = screenHeight; //  - 100;

		mSurfaceHolder = getHolder();
		mSurfaceHolder.addCallback(this);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		mPreviewRunning = false;
	
		
	}

	public PhotoView(Context context) {
		super(context);

		this.context = context;
		mSurfaceHolder = getHolder();
		mSurfaceHolder.addCallback(this);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		//Log.e(TAG, "PhotoView::Surface Created.");
		camera = Camera.open();
		try {
			camera.setPreviewDisplay(holder);

		} catch (IOException exception) {
			camera.release();
			camera = null;
			Log.e(TAG, "surfaceCreated():IO: " + exception.getStackTrace());
			// TODO: add more exception handling logic here
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

		if (mPreviewRunning) {
			camera.stopPreview();
		}
		Camera.Parameters parameters = camera.getParameters();

		// parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
		
	

		// Only API Level 8, Android 2.2
		//camera.setDisplayOrientation(90);
		
		camera.setParameters(parameters);

		try {
			camera.setPreviewDisplay(holder);
		} catch (IOException e) {
			Log.e(TAG, "surfaceChanged():IO:setPreviewDisplay: " + e.getStackTrace());
		}
		
		
		
		try {
			camera.startPreview();
			mPreviewRunning = true;
		} catch (Exception e) {
			Log.e(TAG, "surfaceCreated():IO:startPreview: " + e.getStackTrace());
		}
		
		// auto focus
		Camera.AutoFocusCallback cb = new Camera.AutoFocusCallback() {

			@Override
			public void onAutoFocus(boolean success, Camera camera) {
				//writeToast("Focused");

			}
		};

		camera.autoFocus(cb);
		
		
		// notify user where to click for taking picture.
		Toast.makeText(context,
				"Press CENTER key to take picture", Toast.LENGTH_SHORT).show();
		

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// Surface will be destroyed when we return, so stop the preview.
		// Because the CameraDevice object is not a shared resource, it's very
		// important to release it when the activity is paused.
		camera.stopPreview();
		camera.release();
		camera = null;
		mPreviewRunning = false;
	}

	public void takePic() {
		try {
			camera.takePicture(shutterCallback, rawCallback, jpegCallback);
		} catch (Exception e) {
			Log.e(TAG, "takePic():: " + e.getStackTrace());
		}
	}

	// callbacks
	ShutterCallback shutterCallback = new ShutterCallback() {

		@Override
		public void onShutter() {
			// TODO Auto-generated method stub
			// writeToast("Picture data lost");

		}
	};

	PictureCallback rawCallback = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
		}
	};

	PictureCallback jpegCallback = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			try {

				// prepare IPC data 
				Intent pic = new Intent();
				pic.putExtra("pic", data);
				// prepare result
				act.setResult(Activity.RESULT_OK, pic);
				// finish this activity
				act.finish();
				
				
			} catch (Exception e) {
				Log.e(TAG, "onPictureTaken():: " + e.getStackTrace());
			}
		}
	};

	private void writeToast(String string) {
		Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
	}

	
	@Override
	public void onDraw(Canvas c) {
		c.rotate(90);
	}
	
	
	

}
