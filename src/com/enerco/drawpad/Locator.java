

package com.enerco.drawpad;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import java.text.SimpleDateFormat;

import com.enerco.drawpad.MyLocation.LocationResult;
import com.enerco.drawpad.dialogs.NoteDialog;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnKeyListener;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.location.Location;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import android.text.ClipboardManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Locator extends MapActivity {

	public static final String PREFS_NAME = "DrawpadMapsSettings";
	private static final int DIALOG_EDIT_NOTE = 0;
	protected static final String TAG = "DrawPadMaps ";

	GeoPoint point;
	MapController mapcontrol;
	MarkerOverlay mo;
	MarkerOverlay mymo;
	MyLocation fix;
	MapView map;
	Bitmap map_shot;

	String note;

	Activity act;
	
	// satellite imagery
	Boolean satView = false;

	// Prefs
	SharedPreferences prefs;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maps);

		map = (MapView) findViewById(R.id.mapview);
		// map.setBuiltInZoomControls(true);

		// touch markers
		Drawable marker = this.getResources().getDrawable(R.drawable.pin_32);
		mo = new MarkerOverlay(marker, true, this);
		// pass reference for "edit marker note" dialog
		mo.setCaller(this);

		// my location markers.
		Drawable myMarker = this.getResources().getDrawable(
				R.drawable.pin_blue_32);
		mymo = new MarkerOverlay(myMarker, false, this);
		// pass reference for copy text dialog.
		mymo.setCaller(this);

		List<Overlay> overlays = map.getOverlays();
		overlays.add(mo);
		overlays.add(mymo);

		// get map controller.
		mapcontrol = map.getController();
		mapcontrol.setZoom(15);

		// let's get location data
		fix = new MyLocation();
		// finally load the ad
		// Look up the AdView as a resource and load a request.
		//adView = (AdView) this.findViewById(R.id.ad);
		//adView.setEnabled(true);
		//req = new AdRequest();
		// req.setTesting(true);

		prefs = getSharedPreferences(PREFS_NAME, 0);

		act = this;
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		// notify user with short instructions
		Toast.makeText(this.getApplicationContext(), getResources().getString(R.string.loc_act_show),
				Toast.LENGTH_SHORT).show();
		


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mapmenu, menu);
		return true;

	}

	// menu click handler
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.myloc:
			startLoc();
			return true;
		case R.id.clear:
			clearMarkers();
			return true;
		case R.id.satellite:
			toggleSatelliteView();
			return true;
		case R.id.done:
			completed();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	//	fb.authorizeCallback(requestCode, resultCode, data);
	}

	private void toggleSatelliteView() {

		if (!satView) {
			satView = true;
			map.setSatellite(true);
			map.invalidate();
		} else if (satView) {
			satView = false;
			map.setSatellite(false);
			map.invalidate();
		}

	}


	private void shareMap() {

		// create intent and fill data
		Uri uri = Uri.parse("file:///mnt/sdcard/Pictures/map.png");
		Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		// Intent intent = new Intent("android.intent.action.SEND_MSG");
		intent.putExtra(Intent.EXTRA_STREAM, uri);
		intent.setType("image/png");
		intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		intent.putExtra(Intent.EXTRA_SUBJECT, "Location");
		startActivity(Intent.createChooser(intent, getResources().getString(R.string.loc_share)));
	}

	private void takeMapShot() {

		map_shot = Bitmap.createBitmap(map.getWidth(), map.getHeight(),
				Bitmap.Config.RGB_565);
		map.draw(new Canvas(map_shot));

	}

	private byte[] getByteArrayFromBitmap() {

		takeMapShot();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// map_shot is the bitmap object
		map_shot.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		return baos.toByteArray();

	}

	private Boolean saveFile() {

		//File file = new File(path, "map.png");
		File file = getEndPath();
		
		Log.i(TAG, "Path: " + file.toURI());
		
		
		try {
			FileOutputStream fileOutStream = new FileOutputStream(file);
			BufferedOutputStream bos = new BufferedOutputStream(fileOutStream);

			map_shot.compress(CompressFormat.PNG, 0, bos);

			bos.flush();
			bos.close();
			return true;

		} catch (FileNotFoundException e) {
			Log.e(TAG, "SaveFile()::FNF: " + e.getStackTrace());
			return false;

		} catch (IOException e) {
			Log.e(TAG, "SaveFile()::IO: " + e.getStackTrace());
			return false;

		}

	}

	// method returns path to store temporary file and creates dirs if needed.
	private File getEndPath() {
		
		File path = null;
		
		// use argument if exists
		path = Environment.getExternalStorageDirectory();
		
		// check SD card paths for different vendors

		if (android.os.Build.DEVICE.contains("Samsung")
				|| android.os.Build.MANUFACTURER.contains("Samsung")) {

			path = new File(path, "external_sd/");
		}
				
		path = new File(path, "Pictures/");
		
		if (!path.exists()) {
			Log.i(TAG, "Directory not found, creating Pictures folder");
			path.mkdirs();
		}

		path = new File(path, "map.png");
		
		return path;
	}

	private void clearMarkers() {

		mo.clearMarkers();
		mymo.clearMarkers();

		map.invalidate();
	}

	public void startLoc() {

		fix.getLocation(this, new LocationResult() {

			// callback when fix found!
			@Override
			public void gotLocation(Context context, Location loc) {

				if (loc != null) {
					double lat = loc.getLatitude();
					double lng = loc.getLongitude();

					point = new GeoPoint((int) (lat * 1000000),
							(int) (lng * 1000000));

					Log.d(TAG, "Fix: " + (double) point.getLatitudeE6()
							/ 1000000 + ", " + (double) point.getLongitudeE6()
							/ 1000000);

					mapcontrol.animateTo(point);

					// get time
					String strDate = new SimpleDateFormat("MM/dd@HH:mm")
							.format(new Date());

					// add marker
					OverlayItem overlayitem = new OverlayItem(point,
							getResources().getString(R.string.loc), strDate);
					mymo.addOverlay(overlayitem);
				}

			}
		});
	}

	public void displayNoteDialog() {
		showDialog(DIALOG_EDIT_NOTE);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		switch (id) {
		case DIALOG_EDIT_NOTE:
			// save file dialog
			dialog = editNoteDialog();
			break;

		default:
			dialog = null;
		}
		return dialog;
	}

	// create custom dialog for editing markers.
	private Dialog editNoteDialog() {

		final NoteDialog dlg = new NoteDialog(this);
		dlg.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface arg0) {
				// TODO Auto-generated method stub

				if (!dlg.cancelled) {
					mo.drawNewMarker(note);
					map.invalidate();
				}
	}

		});

		// fetch key: if back pressed, cancel everything...
		
		dlg.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				
				if (keyCode == KeyEvent.KEYCODE_BACK) {

					dlg.cancelled = true;
					dlg.cancel();
					return true;
				}
				
				return false;
			}
		});
		
		
		
		dlg.setContentView(R.layout.edit_note_dialog);
		dlg.setTitle(getResources().getString(R.string.edit_marker));

		Button save = (Button) dlg.findViewById(R.id.dialogButtonOk);
		save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				EditText editBox = (EditText) dlg.findViewById(R.id.dialogEdit);
				setNote(editBox.getText().toString());
				dlg.cancelled = false;
				dlg.dismiss();

			}
		});

		return dlg;

	}

	private void setNote(String str) {
		this.note = str;
	}

	private void shareMMS() {
		// save to file

		try {
			Uri uri = Uri.parse("file:///mnt/sdcard/Pictures/map.png");
			Intent intent = new Intent("android.intent.action.SEND_MSG");
			intent.putExtra(Intent.EXTRA_STREAM, uri);
			intent.setType("image/png");
			intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			intent.putExtra("sms_body", getResources().getString(R.string.loc));
			startActivity(Intent.createChooser(intent, getResources().getString(R.string.post_loc)));
		} catch (Exception e) {
			// Something went wrong with sending MMS.
			Toast.makeText(this.getApplicationContext(),
					getResources().getString(R.string.error_mms), Toast.LENGTH_SHORT)
					.show();
			Log.e(TAG, "shareMMS(): " + e.getStackTrace());
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);

		// Log.i(TAG, "Saving instance...");

		OverlaySerializer os;

		// my location markers
		ArrayList<OverlayItem> mymarkers = mymo.getOverlays();
		int mylength = mymarkers.size();
		os = new OverlaySerializer(mymarkers);
		os.convertToSerializable();

		outState.putInt("MY_MARKERS_SIZE", mylength);
		outState.putByteArray("MY_MARKERS", os.serialize());

		// custom markers
		ArrayList<OverlayItem> customMarkers = mo.getOverlays();
		int customLength = customMarkers.size();
		os = new OverlaySerializer(customMarkers);
		os.convertToSerializable();

		outState.putInt("CUSTOM_MARKERS_SIZE", customLength);
		outState.putByteArray("CUSTOM_MARKERS", os.serialize());

	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState != null) {
			// do everything, including magic stuff

			// my location markers
			OverlaySerializer os;

			// fetch data
			byte[] mymarkers = savedInstanceState.getByteArray("MY_MARKERS");
			int mylength = savedInstanceState.getInt("MY_MARKERS_SIZE");
			// initialize deserializer
			os = new OverlaySerializer(mymarkers, mylength);
			// deserialize
			os.deserialize();
			// fetch results
			ArrayList<OverlayItem> myMapableMarkers = os.convertToMapable();
			// draw results
			mymo.setOverlays(myMapableMarkers);

			// custom markers
			byte[] customMarkers = savedInstanceState
					.getByteArray("CUSTOM_MARKERS");
			int customLength = savedInstanceState.getInt("CUSTOM_MARKERS_SIZE");
			// initialize deserializer
			os = new OverlaySerializer(customMarkers, customLength);
			// deserialize
			os.deserialize();
			// fetch results
			ArrayList<OverlayItem> mapableCustomMarkers = os.convertToMapable();
			// draw results
			mo.setOverlays(mapableCustomMarkers);

		}
	}

	public void addToClipboard(String str) {

		ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		try {
			clip.setText(str);
		} catch (Exception e) {
			Log.e(TAG, "Locator::addToClipboard()::Exception");
			Log.e(TAG, e.getMessage());
		}
	}

	public void printToast(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	//	Intent svcStop = new Intent(Locator.this, PosterService.class);
	//	stopService(svcStop);

	//	fbRunner = null;
	//	fb = null;
	}
	
	public void completed() {

		// take map screenshot
		takeMapShot();
		// save to png.
		Boolean ok = saveFile();
		// share
		if (ok) {
			act.setResult(Activity.RESULT_OK);
			act.finish();
		} else if (!ok)
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_sdcard),
					Toast.LENGTH_LONG).show();

	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// Toast.makeText(getApplicationContext(),
		// "Key: " + keyCode, Toast.LENGTH_SHORT).show();
		
		if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			// center key pressed. Consider operation done.
			completed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}